package Specs;

import Config.Constants;
import Config.Credentials;
import Config.Environment;
import Pages.Common.Common;
import Pages.Kadin.KadinPage;
import Pages.Landing.LandingPage;
import Utils.Helpers;
import Utils.LoadChecker;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CheckImageLoadState {
    WebDriver driver;
    WebDriverWait wait;
    LandingPage landingPage;
    KadinPage kadinPage;

    Map<String, String> kadinBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> erkekBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> cocukBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> ayakkabiCantaBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> saatAksesuarBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> kozmetikBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> evYasamBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> elektronikBoutiqueFailedImages = new HashMap<String, String>();
    Map<String, String> supermarketBoutiqueFailedImages = new HashMap<String, String>();

    @Parameters({"browser"})
    @BeforeTest
    public void beforeTest(String browser) throws MalformedURLException {
        if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }


        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Constants.TIMEOUT, TimeUnit.MILLISECONDS);
        wait = new WebDriverWait(driver, Constants.TIMEOUT);

        landingPage = new LandingPage(driver);
        kadinPage = new KadinPage(driver);
        Common.navigateToEnvironment(driver, Environment.TARGET_ENVIRONMENT);
        landingPage.login(Credentials.TEST_USERNAME, Credentials.TEST_PASSWORD);
    }

    @Test
    public void checkKadinBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Kadın Boutique...");
        this.landingPage.clickMenuItem1();
        kadinBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Kadın Boutique ", kadinBoutiqueFailedImages);
    }


    @Test
    public void checkErkekBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Erkek Boutique");
        this.landingPage.clickMenuItem2();
        erkekBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Erkek Boutique ", erkekBoutiqueFailedImages);
    }

    @Test
    public void checkCocukBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Çocuk Boutique");
        this.landingPage.clickMenuItem3();
        cocukBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Çocuk Boutique ", cocukBoutiqueFailedImages);
    }

    @Test
    public void checkAyakkabiCantaBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Ayakkabı & Çanta Boutique");
        this.landingPage.clickMenuItem4();
        ayakkabiCantaBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Ayakkabı & Çanta Boutique ", ayakkabiCantaBoutiqueFailedImages);
    }

    @Test
    public void checkSaatAksesuarBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Saat & Aksesuar Boutique");
        this.landingPage.clickMenuItem5();
        saatAksesuarBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Saat & Aksesuar Boutique ", saatAksesuarBoutiqueFailedImages);
    }

    @Test
    public void checkKozmetikBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Kozmetik Boutique");
        this.landingPage.clickMenuItem6();
        kozmetikBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Kozmetik Boutique ", kozmetikBoutiqueFailedImages);
    }

    @Test
    public void checkEvYasamBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Ev & Yaşam Boutique");
        this.landingPage.clickMenuItem7();
        evYasamBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Ev & Yaşam Boutique ", evYasamBoutiqueFailedImages);
    }

    @Test
    public void checkElektronikBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Elektronik Boutique");
        this.landingPage.clickMenuItem8();
        elektronikBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Elektronik Boutique ", elektronikBoutiqueFailedImages);
    }

    @Test
    public void checkSupermarketBoutiqueImageLoadStates() throws InterruptedException {
        System.out.println("Checking Süpermarket Boutique");
        this.landingPage.clickMenuItem9();
        supermarketBoutiqueFailedImages = LoadChecker.getFailedImages(driver);
        Helpers.reportFailedImages("Süpermarket Boutique ", supermarketBoutiqueFailedImages);
    }

    @Test
    public void addItemToCart() {
        System.out.println("Adding an item to the shopping cart");
        this.landingPage.clickMenuItem5();
        Common.clickRandomBigBoutique(driver);
        Common.clickRandomProduct(driver);
        Common.clickAddToCartButton(driver);
        Assert.assertEquals(Common.returnBasketCount(driver), "1");
    }

    @AfterTest
    public void destruct() {
        driver.close();
        driver.quit();
    }
}
