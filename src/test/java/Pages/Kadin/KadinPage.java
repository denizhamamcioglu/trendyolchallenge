package Pages.Kadin;

import Config.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class KadinPage {
    WebDriver driver;
    WebDriverWait wait;

    public KadinPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, Constants.TIMEOUT);
    }
}
