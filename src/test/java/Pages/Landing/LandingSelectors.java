package Pages.Landing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LandingSelectors {
    // Reason behind the naming convention: Since main category item orders or names can change, this will be a more dynamic option,
    // more prone to future changes.

    // Menu Items
    public static String mainMenuItem1Id = "item1"; // KADIN
    public static String mainMenuItem2Id = "item2"; // ERKEK
    public static String mainMenuItem3Id = "item3"; // ÇOCUK
    public static String mainMenuItem4Id = "item4"; // AYAKKABI & ÇANTA
    public static String mainMenuItem5Id = "item5"; // SAAT & AKSESUAR
    public static String mainMenuItem6Id = "item6"; // KOZMETİK
    public static String mainMenuItem7Id = "item7"; // EV & YAŞAM
    public static String mainMenuItem8Id = "item8"; // ELEKTRONİK
    public static String mainMenuItem9Id = "item9"; // SÜPERMARKET
    public static String loginButtonClass = "login-register-button-container";
    public static String myAccountLabelId = "logged-in-container";
    public static String loginLabelId = "not-logged-in-container";
    public static String footerAreaClass = "footerBandInfo";
    public static String loginPopupId = "commonPopupRoot";
    public static String firstPopUpContainerId = "popupContainer";

    // Login Frame
    public static String loginIFrameId = "qualaroo_dnt_frame";
    public static String emailFieldId = "email";
    public static String passwordFieldId = "password";
    public static String loginSubmitButtonId = "loginSubmit";

    // Buttons
    public static String closeButtonCss = "[title='Close']";
    public static String backToTopButtonClass = "backToTop";

}
