package Pages.Landing;

import Config.Constants;
import Pages.Common.Common;
import Utils.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LandingPage {
    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;

    public LandingPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, Constants.TIMEOUT);
    }

    // Clicks
    public void clickMenuItem1() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem1Id)).click();
        Helpers.waitUntilPageToLoad(driver);
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);

    }

    public void clickMenuItem2() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem2Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickMenuItem3() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem3Id)).click();
    }

    public void clickMenuItem4() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem4Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickMenuItem5() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem5Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickMenuItem6() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem6Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickMenuItem7() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem7Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickMenuItem8() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem8Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickMenuItem9() {
        driver.findElement(By.id(LandingSelectors.mainMenuItem9Id)).click();
        Assert.assertEquals(Common.isMainContainerDisplayed(driver), true);
    }

    public void clickLoginSubmitButton() {
        driver.findElement(By.id(LandingSelectors.loginSubmitButtonId)).click();
    }

    public void clickLandingModalCloseButton() {
        driver.findElement(By.cssSelector(LandingSelectors.closeButtonCss)).click();
    }

    public void clickLandingLoginButton() {
        driver.findElement(By.id(LandingSelectors.loginLabelId)).click();
    }

    public void clickBackToTopButton() {
        driver.findElement(By.className(LandingSelectors.backToTopButtonClass)).click();
    }

    // Data Entries
    public void enterEmail(String email) {
        WebElement loginField = driver.findElement(By.id(LandingSelectors.emailFieldId));
        loginField.clear();
        loginField.sendKeys(email);
    }

    public void enterPassword(String password) {
        WebElement passwordField = driver.findElement(By.id(LandingSelectors.passwordFieldId));
        passwordField.clear();
        passwordField.sendKeys(password);
    }


    // Checks
    public boolean isLoginSuccessful() {
        return driver.findElement(By.id(LandingSelectors.myAccountLabelId)).isDisplayed();
    }

    // Helper methods
    public void login(String email, String password) {
        clickLandingModalCloseButton();
        Helpers.waitUntilPageToLoad(driver);
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.id(LandingSelectors.firstPopUpContainerId))));
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(LandingSelectors.loginLabelId))));
        clickLandingLoginButton();
        Helpers.waitUntilPageToLoad(driver);
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(LandingSelectors.emailFieldId))));
        enterEmail(email);
        enterPassword(password);
        clickLoginSubmitButton();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.id(LandingSelectors.myAccountLabelId))));
    }
}
