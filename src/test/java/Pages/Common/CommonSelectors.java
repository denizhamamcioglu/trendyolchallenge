package Pages.Common;

public class CommonSelectors {
    public static String mainContainerDivId = "container";
    public static String itemPriceContainerClass = "price-container";
    public static String addToCartButtonClass = "add-to-bs-tx";
    public static String addedToCartButtonClass = "add-to-bs-tx-sc";
    public static String itemDescriptionDivClass = "pr-in-cn";
    public static String largeBoutiqueClass = "butik-large-image";
    public static String basketItemCountId = "basketItemCount";
}
