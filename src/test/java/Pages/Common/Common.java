package Pages.Common;

import Config.Constants;
import Utils.DataGenerator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public final class Common {
    public static void navigateToEnvironment(WebDriver driver, String environment) {
        driver.get(environment);
    }

    public static boolean isMainContainerDisplayed(WebDriver driver) {
        return driver.findElement(By.id(CommonSelectors.mainContainerDivId)).isDisplayed();
    }

    public static void clickPriceContainer(WebDriver driver, int index) {
        driver.findElements(By.className(CommonSelectors.itemPriceContainerClass)).get(index).click();
    }

    public static void clickAddToCartButton(WebDriver driver) {
        driver.findElement(By.className(CommonSelectors.addToCartButtonClass)).click();
    }

    public static boolean isItemDescriptionDisplayed(WebDriver driver) {
        return driver.findElement(By.className(CommonSelectors.itemDescriptionDivClass)).isDisplayed();
    }

    public static void clickRandomBigBoutique(WebDriver driver) {
        int randomIndex = DataGenerator.generateRandomNumberBetween(5, 1);

        List<WebElement> bigBoutiqueDivs = driver.findElements(By.className(CommonSelectors.largeBoutiqueClass));
        bigBoutiqueDivs.get(randomIndex).click();
        new WebDriverWait(driver, Constants.TIMEOUT).until(ExpectedConditions.visibilityOf(driver.findElement(By.className(CommonSelectors.itemPriceContainerClass))));
    }

    public static void clickRandomProduct(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        int randomIndex = DataGenerator.generateRandomNumberBetween(5, 1);
        List<WebElement> itemPriceContainers = driver.findElements(By.className(CommonSelectors.itemPriceContainerClass));
        wait.until(ExpectedConditions.elementToBeClickable(itemPriceContainers.get(randomIndex)));
        itemPriceContainers.get(randomIndex).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(CommonSelectors.itemDescriptionDivClass))));
    }

    public static String returnBasketCount(WebDriver driver) {
        return driver.findElement(By.id(CommonSelectors.basketItemCountId)).getText();
    }

}
