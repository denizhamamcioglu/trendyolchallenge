package Config;

public final class Constants {
    public static int TIMEOUT = 30;
    public static int SPACE_KEY_INTERVAL = 500;
    public static int SCROLL_SPEED = 100;
}
