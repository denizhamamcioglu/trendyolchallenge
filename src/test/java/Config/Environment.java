package Config;

public final class Environment {
    public static final String PROD_URL = "https://www.trendyol.com/";

    public static final String TARGET_ENVIRONMENT = PROD_URL;
}
