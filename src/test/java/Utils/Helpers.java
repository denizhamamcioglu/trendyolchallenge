package Utils;

import Config.Constants;
import Pages.Landing.LandingSelectors;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Helpers {
    static WebDriverWait wait;
    static JavascriptExecutor javascriptExecutor;

    public static void scrollDown(WebDriver driver, int amount) {
        ((JavascriptExecutor) driver).executeScript(String.format("window.scrollBy(0, %s)", amount));
    }

    public static void scrollDownToBottom(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.getElementById('ng-app').scrollHeight)");
    }

    public static void scrollDownGradually(WebDriver driver, int speed) {
        int currentScrollPosition = 0;
        long currentHeight = 1;

        while(currentScrollPosition <= currentHeight) {
            currentScrollPosition += speed;
            ((JavascriptExecutor) driver).executeScript(String.format("window.scrollTo(0, %s)", currentScrollPosition));
            currentHeight = (long) ((JavascriptExecutor) driver).executeScript("return document.getElementById('ng-app').scrollHeight");
        }
    }

    public static List<WebElement> filterBoutiqueImages(WebDriver driver, List<WebElement> images) {
        List<WebElement> boutiqueImages = new ArrayList<WebElement>();
        for (int i=0; i<images.size(); i++) {
            
            String imageClassName = images.get(i).getAttribute("class");
            if (imageClassName.contains("littleBoutiqueImage") || imageClassName.contains("bigBoutiqueImage")) {
                boutiqueImages.add(images.get(i));
            }
        }

        return boutiqueImages;
    }

    public static void waitUntilPageToLoad(WebDriver driver) {
        javascriptExecutor = (JavascriptExecutor) driver;
        new WebDriverWait(driver, Constants.TIMEOUT).until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public static void waitUntilAjaxLoadComplete(WebDriver driver) {
        javascriptExecutor = (JavascriptExecutor) driver;
        new WebDriverWait(driver, Constants.TIMEOUT).until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return jQuery.active").equals(0));
    }

    public static Object returnPagePerformanceEntries(WebDriver driver) {
        return ((JavascriptExecutor) driver).executeScript("");
    }

    public static void waitUntilDomContentLoad(WebDriver driver) {
        javascriptExecutor = (JavascriptExecutor) driver;
        new WebDriverWait(driver, Constants.TIMEOUT).until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public static void reportFailedImages(String prefix, Map<String, String> failedImages) {
        System.out.println(prefix + " Failed Images:");
        System.out.println("Failed image count: " + failedImages.size());
        for (String key : failedImages.keySet()) {
            System.out.println("Image Title: " + key);
            System.out.println("Image Source: " + failedImages.get(key));
        }

        System.out.println("\n\n");
    }
}
