package Utils;

import java.util.Random;

public final class DataGenerator {
    public static int generateRandomNumberBetween(int max, int min) {
        return new Random().nextInt((max-min) + 1) + min;
    }
}
