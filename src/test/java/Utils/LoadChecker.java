package Utils;

import Config.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class LoadChecker {
    WebDriverWait wait;

    public static boolean checkImageLoadState(WebDriver driver, WebElement targetElement) {
        String targetImgClassValue = targetElement.getAttribute("class");
        if (targetImgClassValue.contains("loading")) {
            // wait for the image to load
            new WebDriverWait(driver, Constants.TIMEOUT).until(ExpectedConditions.not(ExpectedConditions.attributeContains(targetElement, "class", "loading")));
        }

//        boolean isImageComplete = (boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].complete;", targetElement); // an alternative checking method
        boolean isLoaded = targetImgClassValue.contains("loaded") && !targetImgClassValue.contains("error");

        return isLoaded;
    }

    public static Map<String, String> getFailedImages(WebDriver driver) throws InterruptedException {
        Helpers.waitUntilPageToLoad(driver);
        Helpers.waitUntilDomContentLoad(driver);
        Helpers.scrollDownGradually(driver, Constants.SCROLL_SPEED);

        List<WebElement> images = driver.findElements(By.tagName("img"));
        List<WebElement> boutiqueImages = Helpers.filterBoutiqueImages(driver, images);

        Map<String, String> failedImages = new HashMap<String, String>();

        int imageCount = boutiqueImages.size();

        for (int i = 0; i < imageCount; i++) {
            WebElement focusedImage = boutiqueImages.get(i);

            if (checkImageLoadState(driver, focusedImage) == false) {
                String failedImageTitle = focusedImage.getAttribute("title");
                String failedImageSource = focusedImage.getAttribute("src");
                failedImages.put(failedImageTitle, failedImageSource);
            }
        }

        return failedImages;
    }
}
