# Trendyol Web Browser Automation Assignment - Deniz Hamamcioglu

This repository contains the code and running instructions written by Deniz Hamamcioglu for web browser automation assignment of Trendyol.com

**Technologies used:**
1. Selenium
2. Maven
3. Java 8

Development Environment: Windows 10 - x64

---

## Dependencies
1. Java and Java JRE
2. Apache Maven
3. GIT Core


## Getting Started
Clone the project to a location that you desire by using the command below:
`git clone https://denizhamamcioglu@bitbucket.org/denizhamamcioglu/trendyolchallange_restapi.git`

Or simply download it using the URL: https://denizhamamcioglu@bitbucket.org/denizhamamcioglu/trendyolchallange_restapi.git

## Installation
1. Navigate to the folder that you've cloned the project.
2. Go inside the trendyolchallenge folder.
3. Open up the terminal inside this folder.
4. Enter the following command:

`mvn clean install`

## Executing the Tests with the desired browser
1. Inside the trendyolchallenge folder, enter the following command:

`mvn test -Dbrowser="<browserName>`